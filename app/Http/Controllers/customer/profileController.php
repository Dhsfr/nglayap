<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\UserProfile;
use App\Models\Provinces;
use App\Models\Regencies;
use Auth;
use Hash;
use Carbon\Carbon;

class profileController extends Controller{
  
    public function index(){
        $user = User::find(Auth::user()->id);
        $user_profile = UserProfile::where('id_user', Auth::user()->id)->get();
        $provinceAll = Provinces::all();
        $regencyAll = Regencies::all();

       
        return view('customer.profile.index', compact('user', 'user_profile', 'provinceAll', 'regencyAll'));
    }

    public function getRegency(Request $request){
        $idProvince = $request->get('idProvinsi');
        $regencie = Regencies::where('id_province', $idProvince)->get();
        return json_encode($regencie);
    }
    
    public function update(Request $request, $id){
       
        $detail = User::find($id);
        if($request->file('foto')!=null){
            if($detail->foto!=null){
                $imagepath=public_path().'/assets/img/profile/'.$detail->foto;
                unlink($imagepath);
            }
            $image = $request->file('foto');
            $imageName = $image->getClientOriginalName();
            $fileName = pathinfo($imageName, PATHINFO_FILENAME);
            $ext = $request->file('foto')->getClientOriginalExtension();
            $tgl = Carbon::now()->format('dmYHis');
            $newname = $fileName . $tgl . "." . $ext;
            $image->move(public_path('assets/img/profile/'), $newname); 

        }else if($detail->foto!=null){
            $newname = $detail->foto;
        }else{
            $imageName = null;
            $newname = null;
        }

        $detail->foto = $request->get('foto');
        $detail->name = $request->get('nama');
        $detail->password = Hash::make($request->password1);
        $detail->foto = $newname;

        if($detail->save()){
            $checkProfile = UserProfile::where('id_user', $id)->get();
            if(count($checkProfile) !=0){
                $update = UserProfile::find($checkProfile[0]->id);
                $update->postcode = $request->get('postcode');
                $update->id_regency = $request->get('kabupaten');
                $update->alamat = $request->get('alamat');
                $update->phone = $request->get('phone');
                $update->save();
            }else{
                $insertProfile = ([
                    'id_user' => Auth::user()->id,
                    'postcode' => $request->get('postcode'),
                    'id_regency' => $request->get('kabupaten'),
                    'alamat' => $request->get('alamat'),
                    'phone' => $request->get('phone'),
                ]);
    
                $user_profile = new UserProfile($insertProfile);
                $user_profile->save();
            }
        }
        return redirect(route('profile.index'))->with('status', 'Data Account Berhasil Dirubah');
    }
}
