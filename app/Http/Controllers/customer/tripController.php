<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\wisataRepository;
use App\Repositories\hotelRepository;
use App\Repositories\kulinerRepository;
use App\Repositories\perhitunganRepository;
use App\Repositories\pesananRepository;
use App\Http\Requests\CartStoreRequest;
use App\Models\Range;
use App\Models\Wisata;
use App\Models\Hotel;
use App\Models\Kuliner;
use App\Models\DataPrediksi;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class tripController extends Controller{
    private $wisataRepository, $hotelRepository, $kulinerRepository, $perhitunganRepository, $pesananRepository;


    public function __construct(wisataRepository $wisataRepository, hotelRepository $hotelRepository, kulinerRepository $kulinerRepository, perhitunganRepository $perhitunganRepository, pesananRepository $pesananRepository){
        $this->wisataRepository = $wisataRepository;
        $this->hotelRepository = $hotelRepository;
        $this->kulinerRepository = $kulinerRepository;
        $this->perhitunganRepository = $perhitunganRepository;
        $this->pesananRepository = $pesananRepository;
    }

    public function index(Request $request){
        $lokasi = $this->perhitunganRepository->getLokasi();
        $range = Range::all();
        $page = $request->get('page', 1);
        $limit = 9;
        $wisata = $this->wisataRepository->get($limit);
        $hotel = $this->hotelRepository->get($limit);
        $kuliner = $this->kulinerRepository->get($limit);
        return view('customer.trip.trip', compact('wisata', 'hotel', 'kuliner', 'lokasi', 'range'));
    }

    public function detailWisata($id){
        $detail = Wisata::find($id);
        return view('customer.trip.detailWisata', compact('detail'));
    }

    public function detailHotel($id){
        $detail = Hotel::find($id);
        return view('customer.trip.detailHotel', compact('detail'));
    }

    public function detailKuliner($id){
        $detail = Kuliner::find($id);
        return view('customer.trip.detailKuliner', compact('detail'));
    }

    public function formTour(){
        $lokasi = $this->perhitunganRepository->getLokasi();
        $range = Range::all();
        return view('customer.trip.formTour', compact('lokasi', 'range'));
    }

    public function dostartroute(Request $request){
        $page = $request->get('page', 1);
        $limit = 9;
        $wisata = $this->wisataRepository->get($limit);
        $hotel = $this->hotelRepository->get($limit);
        $kuliner = $this->kulinerRepository->get($limit);
        $lokasi = $this->perhitunganRepository->getLokasi();

        
        $range = Range::all();
       
        
        $nilaistartroute = $this->startTouring(
            $request->lokasiAwal, 
            $request->kategori1,
            $request->kategori2,
            $request->kategori3,
            $request->jarak,
            $request->rentangHarga,
            $request->rating
        );

        $currentPage = Paginator::resolveCurrentPage();
        $col = collect($nilaistartroute);
        $perPage = 9;
        $currentPageItems = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $items = new Paginator($currentPageItems, count($col), $perPage);
        $items->setPath($request->url());
        $items->appends($request->all());
        
        return view('customer.trip.startTour', compact('items',  'lokasi', 'range'));
    }

    public function startTouring($lokasiAwal, $kategori1, $kategori2, $kategori3, $bobotPreferensiJarak, $bobotPreferensiHarga, $bobotPreferensiRating){
        $rangeJarak = Range::where('idKriteria',1)->get();
        $rangeHarga = Range::where('idKriteria',2)->get();
        $rangeRating = Range::where('idKriteria',3)->get();

        if($kategori1 != null && $kategori2 == null && $kategori3 == null){
            $wisata = DataPrediksi::where('idLokasi', $lokasiAwal)->where('idKategoriData',$kategori1)->get();
            $matriks = [];
            $counter = 0;

            foreach($wisata as $wisatas){
                // normalisasi bobot kriteria
                foreach($rangeJarak as $jarak){
                    if($wisatas->jarak <= $jarak->rentang ){
                        $bobotJarak = $jarak->bobot;
                    }
                }
    
                foreach($rangeHarga as $range){
                    if($wisatas->wisata()->first()->harga <= $range->rentang){
                        $bobotHarga = $range->bobot;
                      
                    }
    
                }
    
                foreach($rangeRating as $rating){
                    if($wisatas->wisata()->first()->rating == $rating->rentang){
                        $bobotRating = $rating->bobot;
                       
                    }
                }
                
                // step 1 membuat matriks
                if($counter <= $wisatas->id ){
                    $matriks[$counter][0] = $bobotJarak;
                    $matriks[$counter][1] = $bobotHarga;
                    $matriks[$counter][2] = $bobotRating;
                    $counter++; 
                }  
            }
           
            // menentukan nilai max  jarak
            $recCountArrayJarak = array();
            foreach ($matriks as $matrik) {
                $recCountArrayJarak[] = $matrik['0'];
            }
    
            $maxJarak = max($recCountArrayJarak);
            $minJarak = min($recCountArrayJarak);
            
            // menentukan nilai min harga
            $recCountArrayHarga = array();
            foreach ($matriks as $matrik) {
                $recCountArrayHarga[] = $matrik['1'];
            }
    
            $minHarga = min($recCountArrayHarga);
    
            // menentukan nilai max rating
            $recCountArrayRating = array();
            foreach($matriks as $matrik){
                $recCountArrayRating[] = $matrik['2'];
            }
    
            $maxRating = max($recCountArrayRating);
            
    
            // Step2 menghitung nilai R 
                // Menghitung Nilai R jarak
                $nilaiRJarak = [];
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    if($bobotPreferensiJarak[$j] == 1){
                        foreach($recCountArrayJarak as $recCountArray){
                            $nilaiRJarak[] = $recCountArray/$maxJarak;
                        }
                    }elseif($bobotPreferensiJarak[$j] == 0.2){
                        foreach($recCountArrayJarak as $recCountArray){
                            $nilaiRJarak[] = $minJarak/$recCountArray;
                        }
                    }
                }

                // Menghitung Nilai R Harga
                $nilaiRHarga = [];
                foreach($recCountArrayHarga as $recCountArrayHargaa){
                    $nilaiRHarga[] = $minHarga/$recCountArrayHargaa;
                }
    
                // Menghitung Nilai R Harga
                $nilaiRRating = [];
                foreach($recCountArrayRating as $recCountArrayRatingg){
                    $nilaiRRating[] = $recCountArrayRatingg/$maxRating;
                }
    
    
            // Membuat Matriks R
            $matriksR = [];
            $count = 0;
            foreach($matriks as $matrik){
                $matriksR[$count][0] = $nilaiRJarak;
                $matriksR[$count][1] = $nilaiRHarga;
                $matriksR[$count][2] = $nilaiRRating;
            }

            // Menentukan Nilai V
            if(count($bobotPreferensiJarak) > 1 ){
                $nilaiV = [];
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksR as $matrikss){
                        for($i=0;$i<count($matriks);$i++){
                            $nilaiV[$j][$i] = ($bobotPreferensiJarak[$j] * $matrikss[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikss[1][$i]) + ($bobotPreferensiRating[$j] * $matrikss[2][$i] );
                        } 
            
                    }
                }


                $idWisata = [];
                $jarakWisata = [];
                $namaWisata = [];
    
                foreach($wisata as $wisataa){
                    $idWisata[] = $wisataa->wisata()->first()->id;
                    $namaWisata[] = $wisataa->wisata()->first()->nama;
                    $jarakWisata[] = $wisataa->jarak;
                    
                    
                }
    
                $tempIdWisata = [];
                for($j=0;$j<count($nilaiV);$j++){
                    foreach( $idWisata as $origKey => $value ){
                        for($i=0;$i<count($matriks);$i++){
                            $tempIdWisata[$j][$i+1] = $nilaiV[$j][$i];
                        }
                    } 
                }
       
                for ($j=0; $j < count($tempIdWisata); $j++) { 
                    arsort($tempIdWisata[$j]);
                }
               
                for($j=0;$j<count($tempIdWisata);$j++){
                    for($i=0;$i<count($matriks);$i++){
                        $tempIdWisata[$j][$i+1] = $idWisata[$i];
                    }
                }
                
                $count = 0;
                $indexIdWisata = [];
                for($j=0;$j<count($tempIdWisata);$j++){
                    foreach($tempIdWisata[$j] as $origKey => $value ){
                        $indexIdWisata[$j][$count+1] = $value;
                        if(count($matriks)-1 != $count){
                            $count++;
                        }else{
                            $count = 0;
                        }  
                    } 
                }
    
    
                //  Menghitung Borda
                $duplikat = $indexIdWisata;
                $sem = 0;
                $hasilcount = [];
    
                for($i=0;$i<count($indexIdWisata);$i++){
                    for ($a=1; $a<count($duplikat[$i])+1; $a++) { 
                        $sem = $duplikat[$i][$a];
                        $hasilcount[$i][$a][0] = $duplikat[$i][$a];
                        for ($b=1; $b < count($duplikat[$i])+1; $b++) { 
                            if($sem == $duplikat[$i][$b]){
                                $hasilcount[$i][$a][1][$b] = 1;
                            }
                            else{
                                $hasilcount[$i][$a][1][$b] = 0;
                            }
                        }
                        
                    }
                    sort($hasilcount[$i]);
                }
    
                $final_array = [];
                // $nilaiPeringkat = [];
                // $bobotBorda = count($final_array);
    
                for($i=0; $i < count($hasilcount); $i++){
                    for ($j=0; $j < count($hasilcount[0]); $j++) { 
                            if($i<count($hasilcount)-1){
                            if($hasilcount[$i][$j][0] == $hasilcount[$i+1][$j][0]){
                                $final_array[$j][0] = $hasilcount[$i+1][$j][0];
                                for ($f=1; $f < count($hasilcount[0])+1; $f++) { 
                                    $final_array[$j][1][$f] = $hasilcount[$i][$j][1][$f] + $hasilcount[$i+1][$j][1][$f];
                                   
                                }
                            }
                        }
                        
                    }
                }
               
                // menentukan bobot peringkat
                $bobotBorda = count($final_array);
                $nilaiPeringkat = [];
    
                for($i=0; $i < count($final_array); $i++){
                    for ($f=1; $f < count($final_array)+1; $f++) {
                        $nilaiPeringkat[$i][1][$f] = ($final_array[$i][1][$f]*$bobotBorda);
                        if($bobotBorda != 1){
                            $bobotBorda--;
                        }else{
                            $bobotBorda = count($final_array);
                        }
                    }
                }
    
                for($i=0; $i < count($nilaiPeringkat); $i++){
                    $sumNilaiPeringkat[$i+1] = array_sum($nilaiPeringkat[$i][1]);
                }
    
                $totalNilaiPeringkat = array_sum($sumNilaiPeringkat);
    
                for($i=0; $i < count($nilaiPeringkat); $i++){
                    $bobotNilaiPeringkat[$i+1] = ($sumNilaiPeringkat[$i+1]/$totalNilaiPeringkat);
                }
                
                arsort($bobotNilaiPeringkat);
    
                $arrayIdWisata = 0;
                foreach($idWisata as $valuee ){
                    $bobotNilaiPeringkat[$valuee] = $idWisata[$arrayIdWisata];
                    $arrayIdWisata ++;
                }
    
                foreach($bobotNilaiPeringkat as $temp){
                    $prediksiWisata[] = DataPrediksi::where('idKategoriData', $kategori1)->where('idJenisData',$temp)->get();
                
                }
    
    
            }else{
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksR as $matrikss){
                        for($i=0;$i<count($matriks);$i++){
                            $nilaiV[] = ($bobotPreferensiJarak[$j] * $matrikss[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikss[1][$i]) + ($bobotPreferensiRating[$j] * $matrikss[2][$i] );
                        } 
            
                    }
                }
                
                 //  Menentukan Rangking V
                $idWisata = [];
                $jarakWisata = [];
                $namaWisata = [];
    
                foreach($wisata as $wisataa){
                    
                        $idWisata[] = $wisataa->wisata()->first()->id;
                        $namaWisata[] = $wisataa->wisata()->first()->nama;
                        $jarakWisata[] = $wisataa->jarak;
                    
                }
                
                $hitungV = 0;
                foreach( $idWisata as $origKey => $value ){
                    $tempIdWisata[$value] = $nilaiV[$hitungV];
                    $hitungV ++;
                    
                }
    
    
                arsort($tempIdWisata);
    
            
                $arrayIdWisata = 0;
                foreach($idWisata as $valuee ){
                    $tempIdWisata[$valuee] = $idWisata[$arrayIdWisata];
                    $arrayIdWisata ++;
                }
                
    
                foreach($tempIdWisata as $temp){
                    $prediksiWisata[] = DataPrediksi::where('idKategoriData', $kategori1)->where('idJenisData',$temp)->get();
                
                }

             

              
            }
            return array($prediksiWisata);

        }elseif($kategori1 == null && $kategori2 != null && $kategori3 == null){
            $hotel = DataPrediksi::where('idLokasi', $lokasiAwal)->where('idKategoriData',$kategori2)->get();
            $matriksHotel = [];
            $counterHotel = 0;

            foreach($hotel as $hotels){
                // normalisasi bobot kriteria
                foreach($rangeJarak as $jarak){
                    if($hotels->jarak <= $jarak->rentang){
                        $bobotJarakHotel = $jarak->bobot;
                    }
                }
    
                foreach($rangeHarga as $range){
                    if($hotels->hotel()->first()->harga <= $range->rentang){
                        $bobotHargaHotel = $range->bobot;
                      
                    }
    
                }
    
                foreach($rangeRating as $rating){
                    if($hotels->hotel()->first()->rating == $rating->rentang){
                        $bobotRatingHotel = $rating->bobot;
                       
                    }
                }
                
                // step 1 membuat matriks
                if($counterHotel <= $hotels->id ){
                    $matriksHotel[$counterHotel][0] = $bobotJarakHotel;
                    $matriksHotel[$counterHotel][1] = $bobotHargaHotel;
                    $matriksHotel[$counterHotel][2] = $bobotRatingHotel;
                    $counterHotel++; 
                }  
            }

            // menentukan nilai max  jarak
            $recCountArrayJarakHotel = array();
            foreach ($matriksHotel as $matrikHotel) {
                $recCountArrayJarakHotel[] = $matrikHotel['0'];
            }

            $maxJarakHotel = max($recCountArrayJarakHotel);
            
    
            // menentukan nilai min harga
            $recCountArrayHargaHotel = array();
            foreach ($matriksHotel as $matrikHotel) {
                $recCountArrayHargaHotel[] = $matrikHotel['1'];
            }

            $minHargaHotel = min($recCountArrayHargaHotel);

    
            // menentukan nilai max rating
            $recCountArrayRatingHotel = array();
            foreach($matriksHotel as $matrikHotel){
                $recCountArrayRatingHotel[] = $matrikHotel['2'];
            }
    
            $maxRatingHotel = max($recCountArrayRatingHotel);
            
            // Step2 menghitung nilai R 
                // Menghitung Nilai R jarak
                $nilaiRJarakHotel = [];
                foreach($recCountArrayJarakHotel as $recCountArrayHotel){
                    $nilaiRJarakHotel[] = $recCountArrayHotel/$maxJarakHotel;
                }
            
                // Menghitung Nilai R Harga
                $nilaiRHargaHotel = [];
                foreach($recCountArrayHargaHotel as $recCountArrayHargaaHotel){
                    $nilaiRHargaHotel[] = $minHargaHotel/$recCountArrayHargaaHotel;
                }
    
                // Menghitung Nilai R Harga
                $nilaiRRatingHotel = [];
                foreach($recCountArrayRatingHotel as $recCountArrayRatinggHotel){
                    $nilaiRRatingHotel[] = $recCountArrayRatinggHotel/$maxRatingHotel;
                }


            // Membuat Matriks R
            $matriksRHotel = [];
            $countHotel = 0;
            foreach($matriksHotel as $matrikHotel){
                $matriksRHotel[$countHotel][0] = $nilaiRJarakHotel;
                $matriksRHotel[$countHotel][1] = $nilaiRHargaHotel;
                $matriksRHotel[$countHotel][2] = $nilaiRRatingHotel;
            }

            // Menentukan Nilai V
            if(count($bobotPreferensiJarak) > 1 ){
                $nilaiVHotel = [];
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksRHotel as $matrikssHotel){
                        for($i=0;$i<count($matriksHotel);$i++){
                            $nilaiVHotel[$j][$i] = ($bobotPreferensiJarak[$j] * $matrikssHotel[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikssHotel[1][$i]) + ($bobotPreferensiRating[$j] * $matrikssHotel[2][$i] );
                        } 
            
                    }
                }
    
                $idHotel = [];
                $jarakHotel = [];
                $namaHotel = [];
    
                foreach($hotel as $hotell){
                    $idHotel[] = $hotell->hotel()->first()->id;
                    $namaHotel[] = $hotell->hotel()->first()->nama;
                    $jarakHotel[] = $hotell->jarak;
                    
                }
    
                $tempIdHotel = [];
                for($j=0;$j<count($nilaiVHotel);$j++){
                    foreach($idHotel as $origKey => $valueHotel ){
                        for($i=0;$i<count($matriksHotel);$i++){
                            $tempIdHotel[$j][$i+1] = $nilaiVHotel[$j][$i];
                        }
                    } 
                }
       
                for ($j=0; $j < count($tempIdHotel); $j++) { 
                    arsort($tempIdHotel[$j]);
                }
               
                for($j=0;$j<count($tempIdHotel);$j++){
                    for($i=0;$i<count($matriksHotel);$i++){
                        $tempIdHotel[$j][$i+1] = $idHotel[$i];
                    }
                }
                
                $countHotel = 0;
                $indexIdHotel = [];
                for($j=0;$j<count($tempIdHotel);$j++){
                    foreach($tempIdHotel[$j] as $origKey => $valueHotel ){
                        $indexIdHotel[$j][$countHotel+1] = $valueHotel;
                        if(count($matriksHotel)-1 != $countHotel){
                            $countHotel++;
                        }else{
                            $countHotel = 0;
                        }  
                    } 
                }
    
    
                //  Menghitung Borda
                $duplikatHotel = $indexIdHotel;
                $semHotel = 0;
                $hasilcountHotel = [];
    
                for($i=0;$i<count($indexIdHotel);$i++){
                    for ($a=1; $a<count($duplikatHotel[$i])+1; $a++) { 
                        $semHotel = $duplikatHotel[$i][$a];
                        $hasilcountHotel[$i][$a][0] = $duplikatHotel[$i][$a];
                        for ($b=1; $b < count($duplikatHotel[$i])+1; $b++) { 
                            if($semHotel == $duplikatHotel[$i][$b]){
                                $hasilcountHotel[$i][$a][1][$b] = 1;
                            }
                            else{
                                $hasilcountHotel[$i][$a][1][$b] = 0;
                            }
                        }
                        
                    }
                    sort($hasilcountHotel[$i]);
                }
    
                $final_arrayHotel = [];
    
                for($i=0; $i < count($hasilcountHotel); $i++){
                    for ($j=0; $j < count($hasilcountHotel[0]); $j++) { 
                            if($i<count($hasilcountHotel)-1){
                            if($hasilcountHotel[$i][$j][0] == $hasilcountHotel[$i+1][$j][0]){
                                $final_arrayHotel[$j][0] = $hasilcountHotel[$i+1][$j][0];
                                for ($f=1; $f < count($hasilcountHotel[0])+1; $f++) { 
                                    $final_arrayHotel[$j][1][$f] = $hasilcountHotel[$i][$j][1][$f] + $hasilcountHotel[$i+1][$j][1][$f];
                                }
                            }
                        }
                        
                    }
                }
               
                // menentukan bobot peringkat
                $bobotBordaHotel = count($final_arrayHotel);
                $nilaiPeringkatHotel = [];
    
                for($i=0; $i < count($final_arrayHotel); $i++){
                    for ($f=1; $f < count($final_arrayHotel)+1; $f++) {
                        $nilaiPeringkatHotel[$i][1][$f] = ($final_arrayHotel[$i][1][$f]*$bobotBordaHotel);
                        if($bobotBordaHotel != 1){
                            $bobotBordaHotel--;
                        }else{
                            $bobotBordaHotel = count($final_arrayHotel);
                        }
                    }
                }
    
                for($i=0; $i < count($nilaiPeringkatHotel); $i++){
                    $sumNilaiPeringkatHotel[$i+1] = array_sum($nilaiPeringkatHotel[$i][1]);
                }
    
                $totalNilaiPeringkatHotel = array_sum($sumNilaiPeringkatHotel);
    
                for($i=0; $i < count($nilaiPeringkatHotel); $i++){
                    $bobotNilaiPeringkatHotel[$i+1] = ($sumNilaiPeringkatHotel[$i+1]/$totalNilaiPeringkatHotel);
                }
                
                arsort($bobotNilaiPeringkatHotel);
    
                $arrayIdHotel = 0;
                foreach($idHotel as $valueeHotel ){
                    $bobotNilaiPeringkatHotel[$valueeHotel] = $idHotel[$arrayIdHotel];
                    $arrayIdHotel ++;
                }
    
                foreach($bobotNilaiPeringkatHotel as $tempHotel){
                    $prediksiHotel[] = DataPrediksi::where('idKategoriData', $kategori2)->where('idJenisData',$tempHotel)->get();
                
                }
    
    
            }else{
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksRHotel as $matrikssHotel){
                        for($i=0;$i<count($matriksHotel);$i++){
                            $nilaiVHotel[] = ($bobotPreferensiJarak[$j] * $matrikssHotel[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikssHotel[1][$i]) + ($bobotPreferensiRating[$j] * $matrikssHotel[2][$i] );
                        } 
            
                    }
                }
                
                 //  Menentukan Rangking V
                $idHotel = [];
                $jarakHotel = [];
                $namaHotel = [];
    
                foreach($hotel as $hotell){
                    $idHotel[] = $hotell->hotel()->first()->id;
                    $namaHotel[] = $hotell->hotel()->first()->nama;
                    $jarakHotel[] = $hotell->jarak;
                    
                }
                
                $hitungVHotel = 0;
                foreach( $idHotel as $origKey => $valueHotel ){
                    $tempIdHotel[$valueHotel] = $nilaiVHotel[$hitungVHotel];
                    $hitungVHotel ++;
                    
                }

                arsort($tempIdHotel);

                $arrayIdHotel = 0;
                foreach($idHotel as $valueeHotel ){
                    $tempIdHotel[$valueeHotel] = $idHotel[$arrayIdHotel];
                    $arrayIdHotel ++;
                }
    
                foreach($tempIdHotel as $tempHotel){
                    $prediksiHotel[] = DataPrediksi::where('idKategoriData', $kategori2)->where('idJenisData',$tempHotel)->get();
                
                }
            }
            return array(null,$prediksiHotel);

        }elseif($kategori1 == null && $kategori2 == null && $kategori3 != null){
            $kuliner = DataPrediksi::where('idLokasi', $lokasiAwal)->where('idKategoriData',$kategori3)->get();
            $matriksKuliner = [];
            $counterKuliner = 0;

            foreach($kuliner as $kuliners){
                // normalisasi bobot kriteria
                foreach($rangeJarak as $jarak){
                    if($kuliners->jarak <= $jarak->rentang){
                        $bobotJarakKuliner = $jarak->bobot;
                    }
                }
    
                foreach($rangeHarga as $range){
                    if($kuliners->kuliner()->first()->harga <= $range->rentang){
                        $bobotHargaKuliner = $range->bobot;
                      
                    }
    
                }
    
                foreach($rangeRating as $rating){
                    if($kuliners->kuliner()->first()->rating == $rating->rentang){
                        $bobotRatingKuliner = $rating->bobot;
                       
                    }
                }
                
                // step 1 membuat matriks
                if($counterKuliner <= $kuliners->id ){
                    $matriksKuliner[$counterKuliner][0] = $bobotJarakKuliner;
                    $matriksKuliner[$counterKuliner][1] = $bobotHargaKuliner;
                    $matriksKuliner[$counterKuliner][2] = $bobotRatingKuliner;
                    $counterKuliner++; 
                }  
            }

            // menentukan nilai max  jarak
            $recCountArrayJarakKuliner = array();
            foreach ($matriksKuliner as $matrikKuliner) {
                $recCountArrayJarakKuliner[] = $matrikKuliner['0'];
            }

            $maxJarakKuliner = max($recCountArrayJarakKuliner);
            
    
            // menentukan nilai min harga
            $recCountArrayHargaKuliner = array();
            foreach ($matriksKuliner as $matrikKuliner) {
                $recCountArrayHargaKuliner[] = $matrikKuliner['1'];
            }

            $minHargaKuliner = min($recCountArrayHargaKuliner);

    
            // menentukan nilai max rating
            $recCountArrayRatingKuliner = array();
            foreach($matriksKuliner as $matrikKuliner){
                $recCountArrayRatingKuliner[] = $matrikKuliner['2'];
            }
    
            $maxRatingKuliner = max($recCountArrayRatingKuliner);
            
            // Step2 menghitung nilai R 
                // Menghitung Nilai R jarak
                $nilaiRJarakKuliner = [];
                foreach($recCountArrayJarakKuliner as $recCountArrayKuliner){
                    $nilaiRJarakKuliner[] = $recCountArrayKuliner/$maxJarakKuliner;
                }
            
                // Menghitung Nilai R Harga
                $nilaiRHargaKuliner = [];
                foreach($recCountArrayHargaKuliner as $recCountArrayHargaaKuliner){
                    $nilaiRHargaKuliner[] = $minHargaKuliner/$recCountArrayHargaaKuliner;
                }
    
                // Menghitung Nilai R Harga
                $nilaiRRatingKuliner = [];
                foreach($recCountArrayRatingKuliner as $recCountArrayRatinggKuliner){
                    $nilaiRRatingKuliner[] = $recCountArrayRatinggKuliner/$maxRatingKuliner;
                }


            // Membuat Matriks R
            $matriksRKuliner = [];
            $countKuliner = 0;
            foreach($matriksKuliner as $matrikKuliner){
                $matriksRKuliner[$countKuliner][0] = $nilaiRJarakKuliner;
                $matriksRKuliner[$countKuliner][1] = $nilaiRHargaKuliner;
                $matriksRKuliner[$countKuliner][2] = $nilaiRRatingKuliner;
            }

            // Menentukan Nilai V
            if(count($bobotPreferensiJarak) > 1 ){
                $nilaiVKuliner = [];
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksRKuliner as $matrikssKuliner){
                        for($i=0;$i<count($matriksKuliner);$i++){
                            $nilaiVKuliner[$j][$i] = ($bobotPreferensiJarak[$j] * $matrikssKuliner[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikssKuliner[1][$i]) + ($bobotPreferensiRating[$j] * $matrikssKuliner[2][$i] );
                        } 
            
                    }
                }
    
                $idKuliner = [];
                $jarakKuliner = [];
                $namaKuliner = [];
    
                foreach($kuliner as $kulinerr){
                    $idKuliner[] = $kulinerr->kuliner()->first()->id;
                    $namaKuliner[] = $kulinerr->kuliner()->first()->nama;
                    $jarakKuliner[] = $kulinerr->jarak;
                    
                }
    
                $tempIdKuliner = [];
                for($j=0;$j<count($nilaiVKuliner);$j++){
                    foreach($idKuliner as $origKey => $valueKuliner ){
                        for($i=0;$i<count($matriksKuliner);$i++){
                            $tempIdKuliner[$j][$i+1] = $nilaiVKuliner[$j][$i];
                        }
                    } 
                }
       
                for ($j=0; $j < count($tempIdKuliner); $j++) { 
                    arsort($tempIdKuliner[$j]);
                }
               
                for($j=0;$j<count($tempIdKuliner);$j++){
                    for($i=0;$i<count($matriksKuliner);$i++){
                        $tempIdKuliner[$j][$i+1] = $idKuliner[$i];
                    }
                }
                
                $countKuliner = 0;
                $indexIdKuliner = [];
                for($j=0;$j<count($tempIdKuliner);$j++){
                    foreach($tempIdKuliner[$j] as $origKey => $valueKuliner ){
                        $indexIdKuliner[$j][$countKuliner+1] = $valueKuliner;
                        if(count($matriksKuliner)-1 != $countKuliner){
                            $countKuliner++;
                        }else{
                            $countKuliner = 0;
                        }  
                    } 
                }
    
    
                //  Menghitung Borda
                $duplikatKuliner = $indexIdKuliner;
                $semKuliner = 0;
                $hasilcountKuliner = [];
    
                for($i=0;$i<count($indexIdKuliner);$i++){
                    for ($a=1; $a<count($duplikatKuliner[$i])+1; $a++) { 
                        $semKuliner = $duplikatKuliner[$i][$a];
                        $hasilcountKuliner[$i][$a][0] = $duplikatKuliner[$i][$a];
                        for ($b=1; $b < count($duplikatKuliner[$i])+1; $b++) { 
                            if($semKuliner == $duplikatKuliner[$i][$b]){
                                $hasilcountKuliner[$i][$a][1][$b] = 1;
                            }
                            else{
                                $hasilcountKuliner[$i][$a][1][$b] = 0;
                            }
                        }
                        
                    }
                    sort($hasilcountKuliner[$i]);
                }
    
                $final_arrayKuliner = [];
    
                for($i=0; $i < count($hasilcountKuliner); $i++){
                    for ($j=0; $j < count($hasilcountKuliner[0]); $j++) { 
                            if($i<count($hasilcountKuliner)-1){
                            if($hasilcountKuliner[$i][$j][0] == $hasilcountKuliner[$i+1][$j][0]){
                                $final_arrayKuliner[$j][0] = $hasilcountKuliner[$i+1][$j][0];
                                for ($f=1; $f < count($hasilcountKuliner[0])+1; $f++) { 
                                    $final_arrayKuliner[$j][1][$f] = $hasilcountKuliner[$i][$j][1][$f] + $hasilcountKuliner[$i+1][$j][1][$f];
                                }
                            }
                        }
                        
                    }
                }
               
                // menentukan bobot peringkat
                $bobotBordaKuliner = count($final_arrayKuliner);
                $nilaiPeringkatKuliner = [];
    
                for($i=0; $i < count($final_arrayKuliner); $i++){
                    for ($f=1; $f < count($final_arrayKuliner)+1; $f++) {
                        $nilaiPeringkatKuliner[$i][1][$f] = ($final_arrayKuliner[$i][1][$f]*$bobotBordaKuliner);
                        if($bobotBordaKuliner != 1){
                            $bobotBordaKuliner--;
                        }else{
                            $bobotBordaKuliner = count($final_arrayKuliner);
                        }
                    }
                }
    
                for($i=0; $i < count($nilaiPeringkatKuliner); $i++){
                    $sumNilaiPeringkatKuliner[$i+1] = array_sum($nilaiPeringkatKuliner[$i][1]);
                }
    
                $totalNilaiPeringkatKuliner = array_sum($sumNilaiPeringkatKuliner);
    
                for($i=0; $i < count($nilaiPeringkatKuliner); $i++){
                    $bobotNilaiPeringkatKuliner[$i+1] = ($sumNilaiPeringkatKuliner[$i+1]/$totalNilaiPeringkatKuliner);
                }
                
                arsort($bobotNilaiPeringkatKuliner);
    
                $arrayIdKuliner = 0;
                foreach($idKuliner as $valueeKuliner ){
                    $bobotNilaiPeringkatKuliner[$valueeKuliner] = $idKuliner[$arrayIdKuliner];
                    $arrayIdKuliner ++;
                }
    
                foreach($bobotNilaiPeringkatKuliner as $tempKuliner){
                    $prediksiKuliner[] = DataPrediksi::where('idKategoriData', $kategori3)->where('idJenisData',$tempKuliner)->get();
                
                }

    
            }else{
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksRKuliner as $matrikssKuliner){
                        for($i=0;$i<count($matriksKuliner);$i++){
                            $nilaiVKuliner[] = ($bobotPreferensiJarak[$j] * $matrikssKuliner[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikssKuliner[1][$i]) + ($bobotPreferensiRating[$j] * $matrikssKuliner[2][$i] );
                        } 
            
                    }
                }
                
                 //  Menentukan Rangking V
                $idKuliner = [];
                $jarakKuliner = [];
                $namaKuliner = [];
    
                foreach($kuliner as $kulinerr){
                    $idKuliner[] = $kulinerr->kuliner()->first()->id;
                    $namaKuliner[] = $kulinerr->kuliner()->first()->nama;
                    $jarakKuliner[] = $kulinerr->jarak;
                    
                }
                
                $hitungVKuliner = 0;
                foreach( $idKuliner as $origKey => $valueKuliner ){
                    $tempIdKuliner[$valueKuliner] = $nilaiVKuliner[$hitungVKuliner];
                    $hitungVKuliner ++;
                }

                arsort($tempIdKuliner);

                $arrayIdKuliner = 0;
                foreach($idKuliner as $valueeKuliner ){
                    $tempIdKuliner[$valueeKuliner] = $idKuliner[$arrayIdKuliner];
                    $arrayIdKuliner ++;
                }

    
                foreach($tempIdKuliner as $tempKuliner){
                    $prediksiKuliner[] = DataPrediksi::where('idKategoriData', $kategori3)->where('idJenisData',$tempKuliner)->get();
                
                }
             
            }
           
            return array(null,null,$prediksiKuliner);
            
        }elseif($kategori1 && $kategori2 != null ){
            $wisata = DataPrediksi::where('idLokasi', $lokasiAwal)->where('idKategoriData',$kategori1)->get();
            $matriks = [];
            $counter = 0;
    
            foreach($wisata as $wisatas){
                // normalisasi bobot kriteria
                foreach($rangeJarak as $jarak){
                    if($wisatas->jarak <= $jarak->rentang ){
                        $bobotJarak = $jarak->bobot;
                    }
                }
    
                foreach($rangeHarga as $range){
                    if($wisatas->wisata()->first()->harga <= $range->rentang){
                        $bobotHarga = $range->bobot;
                      
                    }
    
                }
    
                foreach($rangeRating as $rating){
                    if($wisatas->wisata()->first()->rating == $rating->rentang){
                        $bobotRating = $rating->bobot;
                       
                    }
                }
                
                // step 1 membuat matriks
                if($counter <= $wisatas->id ){
                    $matriks[$counter][0] = $bobotJarak;
                    $matriks[$counter][1] = $bobotHarga;
                    $matriks[$counter][2] = $bobotRating;
                    $counter++; 
                }  
            }
           
            // menentukan nilai max  jarak
            $recCountArrayJarak = array();
            foreach ($matriks as $matrik) {
                $recCountArrayJarak[] = $matrik['0'];
            }
    
            $maxJarak = max($recCountArrayJarak);
            $minJarak = min($recCountArrayJarak);
            
            // menentukan nilai min harga
            $recCountArrayHarga = array();
            foreach ($matriks as $matrik) {
                $recCountArrayHarga[] = $matrik['1'];
            }
    
            $minHarga = min($recCountArrayHarga);
    
            // menentukan nilai max rating
            $recCountArrayRating = array();
            foreach($matriks as $matrik){
                $recCountArrayRating[] = $matrik['2'];
            }
    
            $maxRating = max($recCountArrayRating);
            
    
            // Step2 menghitung nilai R 
                // Menghitung Nilai R jarak
                $nilaiRJarak = [];
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    if($bobotPreferensiJarak[$j] == 1){
                        foreach($recCountArrayJarak as $recCountArray){
                            $nilaiRJarak[] = $recCountArray/$maxJarak;
                        }
                    }elseif($bobotPreferensiJarak[$j] == 0.2){
                        foreach($recCountArrayJarak as $recCountArray){
                            $nilaiRJarak[] = $minJarak/$recCountArray;
                        }
                    }
                }

                // Menghitung Nilai R Harga
                $nilaiRHarga = [];
                foreach($recCountArrayHarga as $recCountArrayHargaa){
                    $nilaiRHarga[] = $minHarga/$recCountArrayHargaa;
                }
    
                // Menghitung Nilai R Harga
                $nilaiRRating = [];
                foreach($recCountArrayRating as $recCountArrayRatingg){
                    $nilaiRRating[] = $recCountArrayRatingg/$maxRating;
                }
    
    
            // Membuat Matriks R
            $matriksR = [];
            $count = 0;
            foreach($matriks as $matrik){
                $matriksR[$count][0] = $nilaiRJarak;
                $matriksR[$count][1] = $nilaiRHarga;
                $matriksR[$count][2] = $nilaiRRating;
            }
         
    
            // Menentukan Nilai V
            if(count($bobotPreferensiJarak) > 1 ){
                $nilaiV = [];
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksR as $matrikss){
                        for($i=0;$i<count($matriks);$i++){
                            $nilaiV[$j][$i] = ($bobotPreferensiJarak[$j] * $matrikss[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikss[1][$i]) + ($bobotPreferensiRating[$j] * $matrikss[2][$i] );
                        } 
            
                    }
                }


                $idWisata = [];
                $jarakWisata = [];
                $namaWisata = [];
    
                foreach($wisata as $wisataa){
                    $idWisata[] = $wisataa->wisata()->first()->id;
                    $namaWisata[] = $wisataa->wisata()->first()->nama;
                    $jarakWisata[] = $wisataa->jarak;
                    
                }
    
                $tempIdWisata = [];
                for($j=0;$j<count($nilaiV);$j++){
                    foreach( $idWisata as $origKey => $value ){
                        for($i=0;$i<count($matriks);$i++){
                            $tempIdWisata[$j][$i+1] = $nilaiV[$j][$i];
                        }
                    } 
                }
       
                for ($j=0; $j < count($tempIdWisata); $j++) { 
                    arsort($tempIdWisata[$j]);
                }
               
                for($j=0;$j<count($tempIdWisata);$j++){
                    for($i=0;$i<count($matriks);$i++){
                        $tempIdWisata[$j][$i+1] = $idWisata[$i];
                    }
                }
                
                $count = 0;
                $indexIdWisata = [];
                for($j=0;$j<count($tempIdWisata);$j++){
                    foreach($tempIdWisata[$j] as $origKey => $value ){
                        $indexIdWisata[$j][$count+1] = $value;
                        if(count($matriks)-1 != $count){
                            $count++;
                        }else{
                            $count = 0;
                        }  
                    } 
                }
    
    
                //  Menghitung Borda
                $duplikat = $indexIdWisata;
                $sem = 0;
                $hasilcount = [];
    
                for($i=0;$i<count($indexIdWisata);$i++){
                    for ($a=1; $a<count($duplikat[$i])+1; $a++) { 
                        $sem = $duplikat[$i][$a];
                        $hasilcount[$i][$a][0] = $duplikat[$i][$a];
                        for ($b=1; $b < count($duplikat[$i])+1; $b++) { 
                            if($sem == $duplikat[$i][$b]){
                                $hasilcount[$i][$a][1][$b] = 1;
                            }
                            else{
                                $hasilcount[$i][$a][1][$b] = 0;
                            }
                        }
                        
                    }
                    sort($hasilcount[$i]);
                }
    
                $final_array = [];
                // $nilaiPeringkat = [];
                // $bobotBorda = count($final_array);
    
                for($i=0; $i < count($hasilcount); $i++){
                    for ($j=0; $j < count($hasilcount[0]); $j++) { 
                            if($i<count($hasilcount)-1){
                            if($hasilcount[$i][$j][0] == $hasilcount[$i+1][$j][0]){
                                $final_array[$j][0] = $hasilcount[$i+1][$j][0];
                                for ($f=1; $f < count($hasilcount[0])+1; $f++) { 
                                    $final_array[$j][1][$f] = $hasilcount[$i][$j][1][$f] + $hasilcount[$i+1][$j][1][$f];
                                   
                                }
                            }
                        }
                        
                    }
                }
               
                // menentukan bobot peringkat
                $bobotBorda = count($final_array);
                $nilaiPeringkat = [];
    
                for($i=0; $i < count($final_array); $i++){
                    for ($f=1; $f < count($final_array)+1; $f++) {
                        $nilaiPeringkat[$i][1][$f] = ($final_array[$i][1][$f]*$bobotBorda);
                        if($bobotBorda != 1){
                            $bobotBorda--;
                        }else{
                            $bobotBorda = count($final_array);
                        }
                    }
                }
    
                for($i=0; $i < count($nilaiPeringkat); $i++){
                    $sumNilaiPeringkat[$i+1] = array_sum($nilaiPeringkat[$i][1]);
                }
    
                $totalNilaiPeringkat = array_sum($sumNilaiPeringkat);
    
                for($i=0; $i < count($nilaiPeringkat); $i++){
                    $bobotNilaiPeringkat[$i+1] = ($sumNilaiPeringkat[$i+1]/$totalNilaiPeringkat);
                }
                
                arsort($bobotNilaiPeringkat);
    
                $arrayIdWisata = 0;
                foreach($idWisata as $valuee ){
                    $bobotNilaiPeringkat[$valuee] = $idWisata[$arrayIdWisata];
                    $arrayIdWisata ++;
                }
    
                foreach($bobotNilaiPeringkat as $temp){
                    $prediksiWisata[] = DataPrediksi::where('idKategoriData', $kategori1)->where('idJenisData',$temp)->get();
                
                }
    
    
            }else{
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksR as $matrikss){
                        for($i=0;$i<count($matriks);$i++){
                            $nilaiV[] = ($bobotPreferensiJarak[$j] * $matrikss[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikss[1][$i]) + ($bobotPreferensiRating[$j] * $matrikss[2][$i] );
                        } 
            
                    }
                }
                
                 //  Menentukan Rangking V
                $idWisata = [];
                $jarakWisata = [];
                $namaWisata = [];
    
                foreach($wisata as $wisataa){
                    
                        $idWisata[] = $wisataa->wisata()->first()->id;
                        $namaWisata[] = $wisataa->wisata()->first()->nama;
                        $jarakWisata[] = $wisataa->jarak;
                    
                }
                
                $hitungV = 0;
                foreach( $idWisata as $origKey => $value ){
                    $tempIdWisata[$value] = $nilaiV[$hitungV];
                    $hitungV ++;
                    
                }
    
    
                arsort($tempIdWisata);
    
            
                $arrayIdWisata = 0;
                foreach($idWisata as $valuee ){
                    $tempIdWisata[$valuee] = $idWisata[$arrayIdWisata];
                    $arrayIdWisata ++;
                }
    
                foreach($tempIdWisata as $temp){
                    $prediksiWisata[] = DataPrediksi::where('idKategoriData', $kategori1)->where('idJenisData',$temp)->get();
                
                }
            }

            // hotel
            $hotel = DataPrediksi::where('idLokasi', $lokasiAwal)->where('idKategoriData',$kategori2)->get();
            $matriksHotel = [];
            $counterHotel = 0;

            foreach($hotel as $hotels){
                // normalisasi bobot kriteria
                foreach($rangeJarak as $jarak){
                    if($hotels->jarak <= $jarak->rentang){
                        $bobotJarakHotel = $jarak->bobot;
                    }
                }
    
                foreach($rangeHarga as $range){
                    if($hotels->hotel()->first()->harga <= $range->rentang){
                        $bobotHargaHotel = $range->bobot;
                      
                    }
    
                }
    
                foreach($rangeRating as $rating){
                    if($hotels->hotel()->first()->rating == $rating->rentang){
                        $bobotRatingHotel = $rating->bobot;
                       
                    }
                }
                
                // step 1 membuat matriks
                if($counterHotel <= $hotels->id ){
                    $matriksHotel[$counterHotel][0] = $bobotJarakHotel;
                    $matriksHotel[$counterHotel][1] = $bobotHargaHotel;
                    $matriksHotel[$counterHotel][2] = $bobotRatingHotel;
                    $counterHotel++; 
                }  
            }

            // menentukan nilai max  jarak
            $recCountArrayJarakHotel = array();
            foreach ($matriksHotel as $matrikHotel) {
                $recCountArrayJarakHotel[] = $matrikHotel['0'];
            }

            $maxJarakHotel = max($recCountArrayJarakHotel);
            
    
            // menentukan nilai min harga
            $recCountArrayHargaHotel = array();
            foreach ($matriksHotel as $matrikHotel) {
                $recCountArrayHargaHotel[] = $matrikHotel['1'];
            }

            $minHargaHotel = min($recCountArrayHargaHotel);

    
            // menentukan nilai max rating
            $recCountArrayRatingHotel = array();
            foreach($matriksHotel as $matrikHotel){
                $recCountArrayRatingHotel[] = $matrikHotel['2'];
            }
    
            $maxRatingHotel = max($recCountArrayRatingHotel);
            
            // Step2 menghitung nilai R 
                // Menghitung Nilai R jarak
                $nilaiRJarakHotel = [];
                foreach($recCountArrayJarakHotel as $recCountArrayHotel){
                    $nilaiRJarakHotel[] = $recCountArrayHotel/$maxJarakHotel;
                }
            
                // Menghitung Nilai R Harga
                $nilaiRHargaHotel = [];
                foreach($recCountArrayHargaHotel as $recCountArrayHargaaHotel){
                    $nilaiRHargaHotel[] = $minHargaHotel/$recCountArrayHargaaHotel;
                }
    
                // Menghitung Nilai R Harga
                $nilaiRRatingHotel = [];
                foreach($recCountArrayRatingHotel as $recCountArrayRatinggHotel){
                    $nilaiRRatingHotel[] = $recCountArrayRatinggHotel/$maxRatingHotel;
                }


            // Membuat Matriks R
            $matriksRHotel = [];
            $countHotel = 0;
            foreach($matriksHotel as $matrikHotel){
                $matriksRHotel[$countHotel][0] = $nilaiRJarakHotel;
                $matriksRHotel[$countHotel][1] = $nilaiRHargaHotel;
                $matriksRHotel[$countHotel][2] = $nilaiRRatingHotel;
            }

            // Menentukan Nilai V
            if(count($bobotPreferensiJarak) > 1 ){
                $nilaiVHotel = [];
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksRHotel as $matrikssHotel){
                        for($i=0;$i<count($matriksHotel);$i++){
                            $nilaiVHotel[$j][$i] = ($bobotPreferensiJarak[$j] * $matrikssHotel[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikssHotel[1][$i]) + ($bobotPreferensiRating[$j] * $matrikssHotel[2][$i] );
                        } 
            
                    }
                }
    
                $idHotel = [];
                $jarakHotel = [];
                $namaHotel = [];
    
                foreach($hotel as $hotell){
                    $idHotel[] = $hotell->hotel()->first()->id;
                    $namaHotel[] = $hotell->hotel()->first()->nama;
                    $jarakHotel[] = $hotell->jarak;
                    
                }
    
                $tempIdHotel = [];
                for($j=0;$j<count($nilaiVHotel);$j++){
                    foreach($idHotel as $origKey => $valueHotel ){
                        for($i=0;$i<count($matriksHotel);$i++){
                            $tempIdHotel[$j][$i+1] = $nilaiVHotel[$j][$i];
                        }
                    } 
                }
       
                for ($j=0; $j < count($tempIdHotel); $j++) { 
                    arsort($tempIdHotel[$j]);
                }
               
                for($j=0;$j<count($tempIdHotel);$j++){
                    for($i=0;$i<count($matriksHotel);$i++){
                        $tempIdHotel[$j][$i+1] = $idHotel[$i];
                    }
                }
                
                $countHotel = 0;
                $indexIdHotel = [];
                for($j=0;$j<count($tempIdHotel);$j++){
                    foreach($tempIdHotel[$j] as $origKey => $valueHotel ){
                        $indexIdHotel[$j][$countHotel+1] = $valueHotel;
                        if(count($matriksHotel)-1 != $countHotel){
                            $countHotel++;
                        }else{
                            $countHotel = 0;
                        }  
                    } 
                }
    
    
                //  Menghitung Borda
                $duplikatHotel = $indexIdHotel;
                $semHotel = 0;
                $hasilcountHotel = [];
    
                for($i=0;$i<count($indexIdHotel);$i++){
                    for ($a=1; $a<count($duplikatHotel[$i])+1; $a++) { 
                        $semHotel = $duplikatHotel[$i][$a];
                        $hasilcountHotel[$i][$a][0] = $duplikatHotel[$i][$a];
                        for ($b=1; $b < count($duplikatHotel[$i])+1; $b++) { 
                            if($semHotel == $duplikatHotel[$i][$b]){
                                $hasilcountHotel[$i][$a][1][$b] = 1;
                            }
                            else{
                                $hasilcountHotel[$i][$a][1][$b] = 0;
                            }
                        }
                        
                    }
                    sort($hasilcountHotel[$i]);
                }
    
                $final_arrayHotel = [];
    
                for($i=0; $i < count($hasilcountHotel); $i++){
                    for ($j=0; $j < count($hasilcountHotel[0]); $j++) { 
                            if($i<count($hasilcountHotel)-1){
                            if($hasilcountHotel[$i][$j][0] == $hasilcountHotel[$i+1][$j][0]){
                                $final_arrayHotel[$j][0] = $hasilcountHotel[$i+1][$j][0];
                                for ($f=1; $f < count($hasilcountHotel[0])+1; $f++) { 
                                    $final_arrayHotel[$j][1][$f] = $hasilcountHotel[$i][$j][1][$f] + $hasilcountHotel[$i+1][$j][1][$f];
                                }
                            }
                        }
                        
                    }
                }
               
                // menentukan bobot peringkat
                $bobotBordaHotel = count($final_arrayHotel);
                $nilaiPeringkatHotel = [];
    
                for($i=0; $i < count($final_arrayHotel); $i++){
                    for ($f=1; $f < count($final_arrayHotel)+1; $f++) {
                        $nilaiPeringkatHotel[$i][1][$f] = ($final_arrayHotel[$i][1][$f]*$bobotBordaHotel);
                        if($bobotBordaHotel != 1){
                            $bobotBordaHotel--;
                        }else{
                            $bobotBordaHotel = count($final_arrayHotel);
                        }
                    }
                }
    
                for($i=0; $i < count($nilaiPeringkatHotel); $i++){
                    $sumNilaiPeringkatHotel[$i+1] = array_sum($nilaiPeringkatHotel[$i][1]);
                }
    
                $totalNilaiPeringkatHotel = array_sum($sumNilaiPeringkatHotel);
    
                for($i=0; $i < count($nilaiPeringkatHotel); $i++){
                    $bobotNilaiPeringkatHotel[$i+1] = ($sumNilaiPeringkatHotel[$i+1]/$totalNilaiPeringkatHotel);
                }
                
                arsort($bobotNilaiPeringkatHotel);
    
                $arrayIdHotel = 0;
                foreach($idHotel as $valueeHotel ){
                    $bobotNilaiPeringkatHotel[$valueeHotel] = $idHotel[$arrayIdHotel];
                    $arrayIdHotel ++;
                }
    
                foreach($bobotNilaiPeringkatHotel as $tempHotel){
                    $prediksiHotel[] = DataPrediksi::where('idKategoriData', $kategori2)->where('idJenisData',$tempHotel)->get();
                
                }
    
    
            }else{
                for($j=0;$j<count($bobotPreferensiJarak);$j++){
                    foreach($matriksRHotel as $matrikssHotel){
                        for($i=0;$i<count($matriksHotel);$i++){
                            $nilaiVHotel[] = ($bobotPreferensiJarak[$j] * $matrikssHotel[0][$i]) + ($bobotPreferensiHarga[$j] * $matrikssHotel[1][$i]) + ($bobotPreferensiRating[$j] * $matrikssHotel[2][$i] );
                        } 
            
                    }
                }
                
                 //  Menentukan Rangking V
                $idHotel = [];
                $jarakHotel = [];
                $namaHotel = [];
    
                foreach($hotel as $hotell){
                    $idHotel[] = $hotell->hotel()->first()->id;
                    $namaHotel[] = $hotell->hotel()->first()->nama;
                    $jarakHotel[] = $hotell->jarak;
                    
                }
                
                $hitungVHotel = 0;
                foreach( $idHotel as $origKey => $valueHotel ){
                    $tempIdHotel[$valueHotel] = $nilaiVHotel[$hitungVHotel];
                    $hitungVHotel ++;
                    
                }

                arsort($tempIdHotel);

                $arrayIdHotel = 0;
                foreach($idHotel as $valueeHotel ){
                    $tempIdHotel[$valueeHotel] = $idHotel[$arrayIdHotel];
                    $arrayIdHotel ++;
                }
    
                foreach($tempIdHotel as $tempHotel){
                    $prediksiHotel[] = DataPrediksi::where('idKategoriData', $kategori2)->where('idJenisData',$tempHotel)->get();
                
                }
            }
            return array($prediksiWisata,$prediksiHotel);
        }
    }
}
