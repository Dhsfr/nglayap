<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\Models\Role;

class LoginController extends Controller{

    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::HOME;


    public function __construct(){
        $this->middleware('guest')->except('logout');
    }

    public function showAdminLoginForm(){
        return view('auth.login', ['url' => 'admin']);
    }

    public function loginAdmin(Request $request){
        $test = $this->validate($request, [
            'email'   => 'required',
            'password' => 'required|min:6'
        ]);


        if (Auth::guard('web')->attempt(
                [
                    'email' => $request->email,
                    'password' => $request->password,

                ], $request->get('remember'))) {

            if(Auth::guard('web')->user()->hasRole('admin')){
                return redirect()->intended(route('admin.home'));
            }
        }

        \Session::flash('notif-error', 'Failed to login.');
        return back()->withInput($request->only('email', 'remember'));
    }


    public function login(Request $request){

        $test = $this->validate($request, [
            'email'   => 'required',
            'password' => 'required|min:6'
        ]);


        if (Auth::guard('web')->attempt(
                [
                    'email' => $request->email,
                    'password' => $request->password,

                ], $request->get('remember'))) {

            if(Auth::guard('web')->user()->hasRole('customer')){
                return redirect()->intended(route('home.index'));
            }
        }

        \Session::flash('notif-error', 'Failed to login.');
        return back()->withInput($request->only('email', 'remember'));
    }
}
