<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataPrediksiStoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'idLokasi' => 'required|integer',
            'idKategoriData' => 'required|integer',
            'idJenisData' => 'required|integer',
            'jarak' => 'required|max:191',
        ];
    }
}
