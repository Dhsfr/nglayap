<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RangeStoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'nama' => 'required|max:191',
            'rentang' => 'required|max:191',
            'bobot' => 'required|max:191',
        ];
    }
}
