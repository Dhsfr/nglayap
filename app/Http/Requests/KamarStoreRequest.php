<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KamarStoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'foto' => 'image|mimes:jpeg,jpg,png',
            'nama' => 'required|max:191',
            'harga' => 'required|max:191',
            'fasilitas' => 'required|max:191',
            'deskripsi' => 'required|max:191',
        ];
    }
}
