<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KulinerUpdateRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'foto' => 'image|mimes:jpeg,jpg,png',
            'nama' => 'required|max:191',
            'harga' => 'required|max:191',
            'alamat' => 'required|max:191',
            'rating' => 'required|max:191',
            'deskripsi' => 'required|max:191',
        ];
    }
}
