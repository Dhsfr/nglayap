<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model{
    public $table = "cart";

    protected $fillable = [
        'idUser', 'idKategoriData', 'idJenisData',
    ];
    
    public function kategoriData(){
        return $this->belongsTo('App\Models\KategoriData', 'idKategoriData', 'id');
    }

    public function wisata(){
        return $this->belongsTo('App\Models\Wisata', 'idJenisData', 'id');
    }

    public function hotel(){
        return $this->belongsTo('App\Models\Hotel', 'idJenisData', 'id');
    }

    public function kuliner(){
        return $this->belongsTo('App\Models\Kuliner', 'idJenisData', 'id');
    }
}
