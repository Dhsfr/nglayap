<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model{
    public $table = "role_user";
    public $primaryKey = "user_id";
    public $timestamps = false;

	protected $fillable = [
		'user_id', 'role_id',
	];

    public function getUserObject(){
        return $this->belongsToMany(User::class)->using(UserRole::class);
    }

    public function user(){
      return $this->belongsTo('App\User', 'id', 'user_id');
    }

    public function admin(){
        return $this->belongsTo('App\Models\Admin', 'id', 'user_id');
      }
}
