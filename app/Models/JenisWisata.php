<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisWisata extends Model{
    protected $fillable = [
      'jenis',  
    ];

    public $timestamps = false;
}
