<?php
namespace App\Repositories;

use App\Models\Kriteria;
use App\Models\Range;
use App\Models\Lokasi;
use App\Models\Wisata;
use App\Models\DataPrediksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class perhitunganRepository{
    private $kriteria, $range, $lokasi, $dataPrediksi;

    public function __construct(Kriteria $kriteria, Range $range, Lokasi $lokasi, DataPrediksi $dataPrediksi){
        $this->kriteria = $kriteria;
        $this->range = $range;
        $this->lokasi = $lokasi;
        $this->dataPrediksi = $dataPrediksi;
    }

    public function getKriteria($with = null, $id = null){
        $kriteria = $this->kriteria
            ->when($with, function($query) use($with){
                return $query->with($with);
            })

            ->when($id, function($query) use($id){
                return $query->where('id', $id);
            });
        
        return $kriteria->get();
    }

    public function storeKriteria(Request $request){
        DB::beginTransaction();

        try{
            $kriteria = Kriteria::create($request->all());
            DB::commit();
            return $kriteria;

        }catch(\Exception $e){
            DB::rollback();
            throw new \Exception($e);
        }
    }

    public function updateKriteria(Request $request, Kriteria $kriteria, $id){
        DB::beginTransaction();

        try{
            $kriteria->where('id', $id)->update([
                'nama' => $request->get('nama'),
                'kategori' => $request->get('kategori'),
                'satuan' => $request->get('satuan'),
            ]);

            DB::commit();
            return $kriteria;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function destroyKriteria($id){
        $kriteria = Kriteria::where('id', $id)->first();
        $kriteria->forceDelete();
        return $kriteria;
    }

    // range
    public function getRange($with = null){
        $range = $this->range
            ->when($with, function($query) use($with){
                return $query->whith($with);
            });

        return $range->get();
    }

    public function createRange(){
        $kriteria = Kriteria::all();
        return $kriteria;
    }

    public function storeRange(Request $request){
        DB::beginTransaction();

        try{
            $range = Range::create($request->all());
            DB::commit();
            return $range;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }

    }

    public function updateRange(Request $request, Range $range, $id){
        DB::beginTransaction();

        try{
            $range->where('id', $id)->update([
                'idKriteria' => $request->get('idKriteria'),
                'nama' => $request->get('nama'),
                'rentang' => $request->get('rentang'),
                'bobot' => $request->get('bobot'),
            ]);

            DB::commit();
            return $range;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function destroyRange($id){
        $range = Range::where('id', $id)->first();
        $range->forceDelete();
        return $range;
    }

    // lokasi awal
    public function getLokasi($with = null, $id = null){
        $lokasi = $this->lokasi
            ->when($with, function($query) use ($with){
                return $query->with($with);
            })
            
            ->when($id, function($query) use($id){
                return $query->where('id', $id);
            });
        
        return $lokasi->get();
    } 

    public function storeLokasi(Request $request){
        DB::beginTransaction();

        try{
            $lokasi = Lokasi::create($request->all());
            DB::commit();
            return $lokasi;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function updateLokasi(Request $request, Lokasi $lokasi, $id){
        DB::beginTransaction();

        try{
            $lokasi->where('id', $id)->update([
                'nama' => $request->get('nama'),
                'alamat' => $request->get('alamat'),
            ]);

            DB::commit();
            return $lokasi;
        
        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function destroyLokasi($id){
        $lokasi = Lokasi::where('id', $id)->first();
        $lokasi->forceDelete();
        return $lokasi;
    }

    // data prediksi
    public function getDataPrediksi($with = null){
        $dataPrediksi = $this->dataPrediksi
            ->when($with, function($query) use ($with){
                return $query->with($with);
            });
        
        return $dataPrediksi->get();
    }

    public function storeDataPrediksi(Request $request){
        DB::beginTransaction();

        try{
            $dataPrediksi = DataPrediksi::create($request->all());
            DB::commit();
            return $dataPrediksi;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function updateDataPrediksi(Request $request, DataPrediksi $dataPrediksi, $id){
        DB::beginTransaction();

        try{
            $dataPrediksi->where('id', $id)->update([
                'idLokasi' => $request->get('idLokasi'),
                'idKategoriData' => $request->get('idKategoriData'),
                'idJenisData' => $request->get('idJenisData'),
                'jarak' => $request->get('jarak'),
            ]);

            DB::commit();
            return $dataPrediksi;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function destroyDataPrediksi($id){
        $dataPrediksi = DataPrediksi::where('id', $id)->first();
        $dataPrediksi->forceDelete();
        return $dataPrediksi;
    }
}