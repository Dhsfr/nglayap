<?php
namespace App\Repositories;
use App\Models\Wisata;
use App\Models\Hotel;
use App\Models\Kuliner;

class touringRepository{
    private $wisata;
    private $hotel;
    private $kuliner;

    public function __construct(Wisata $wisata, Hotel $hotel, Kuliner $kuliner){
        $this->wisata = $wisata;
        $this->hotel = $hotel;
        $this->kuliner = $kuliner;
    }

    public function getWisata($pagination = null, $with = null){
        $wisata = $this->wisata
            ->when($with, function($query) use($with){
                return $query->with($with);
            });

        if($pagination){
            return $wisata->paginate($pagination);
        }

        return $wisata->get();
    }

    public function getHotel($pagination = null, $with = null){
        $hotel = $this->hotel
            ->when($with, function($query) use($with){
                return $query->with($with);
            });

        if($pagination){
            return $hotel->paginate($pagination);
        }

        return $hotel->get();
    }

    public function getKuliner($pagination = null, $with= null){
        $kuliner = $this->kuliner
            ->when($with, function($query) use($with){
                return $query->with($with);
            });

        if($pagination){
            return $kuliner->paginate($pagination);
        }

        return $kuliner->get();
    }

}