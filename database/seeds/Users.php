<?php

use Illuminate\Database\Seeder;
use App\User;



class Users extends Seeder{

    public function run(){

        factory(User::class)->state('admin')->create([
            'email' => 'admin@admin',
        ]);

        factory(User::class)->state('customer')->create([
            'email' => 'customer@customer',
        ]);
    }
}
