<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class Roles extends Seeder{
  
    public function run(){
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'customer']);
        Role::create(['name' => 'hotels']);
    }
}
