<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriData;

class KategoriDatas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriData::create(['nama' => 'Wisata']);
        KategoriData::create(['nama' => 'Hotel']);
        KategoriData::create(['nama' => 'Kuliner']);
    }
}
