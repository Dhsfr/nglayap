<?php

use Illuminate\Database\Seeder;
use App\Models\JenisWisata;

class JenisWisatas extends Seeder{

    public function run(){
        JenisWisata::create(['jenis' => 'Pantai']);
        JenisWisata::create(['jenis' => 'Air Terjun']);
        JenisWisata::create(['jenis' => 'Agrowisata']);
        JenisWisata::create(['jenis' => 'Pemandian']);
        JenisWisata::create(['jenis' => 'Budaya']);
        JenisWisata::create(['jenis' => 'Adventure']);
        JenisWisata::create(['jenis' => 'Religi']);
        JenisWisata::create(['jenis' => 'Buatan']);
    }
}
