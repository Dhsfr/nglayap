@extends('customer.layouts.app')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="home_banner_area" id="trip">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div class="banner_content">
                            <h1>TRAVELING</h1>
                            <p>IS EASY AND FUN</p>
                            <!-- <div class="btn">
                                <div class="nav-item"><a href="{{ route('login') }}" class="sm-btn primary_btn">sign in</a></div>
                                <div class="nav-item"><a href="#" class="sm-btn secondary_btn">sign up</a></div>
                            </div> -->
                        </divc>
                    </div>
                </div>
            </div>
        </div>
    </section><br>
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>My Cart</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="invoice p-3 mb-3">
                            <div class="row">
                                <div class="col-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Qty</th>
                                                <th>Product</th>
                                                <th>Jenis Tiket</th>
                                                <th>Subtotal</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($myCart as $cart)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $cart->jumlah }}</td>
                                                    @if($cart->idKategoriData == 1)
                                                        <td>{{ $cart->wisata()->first()->nama }}</td>
                                                        <td>{{ $cart->kategoriData()->first()->nama }}</td>
                                                        <td>
                                                            @php
                                                                $subTotalWisata = $cart->wisata()->first()->harga * $cart->jumlah;
                                                            @endphp
                                                            {{ $subTotalWisata }}
                                                        </td>
                                                    @elseif($cart->idKategoriData == 2)
                                                        <td>{{ $cart->hotel()->first()->nama }}</td>
                                                        <td>{{ $cart->kategoriData()->first()->nama }}</td>
                                                        <td>
                                                            @php
                                                                $subTotalHotel = $cart->hotel()->first()->harga * $cart->jumlah;
                                                            @endphp
                                                            {{ $subTotalHotel }}
                                                        </td>
                                                    @else
                                                        <td>{{ $cart->kuliner()->first()->nama }}</td>
                                                        <td>{{ $cart->kategoriData()->first()->nama }}</td>
                                                        <td>
                                                            @php
                                                                $subTotalKuliner = $cart->kuliner()->first()->harga * $cart->jumlah;
                                                            @endphp
                                                            {{ $subTotalKuliner }}
                                                        </td>
                                                    @endif

                                                    <td>
                                                        <a href="" data-toggle="modal" data-target="#modal-sm_{{ $cart->id }}"><i class="fa fa-edit"></i></a>
                                                        ||
                                                        <a href="" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin?', 'Konfirmasi Penghapusan Pengguna')){ $('form#hapus{{ $loop->iteration }}').submit(); }"><i class="fa fa-trash"></i></a>
                                                        <form id="hapus{{ $loop->iteration }}" action="{{ route('cart.destroy', $cart->id) }}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                        </form>
                                                    </td>
                                                </tr>
                                                <div class="modal fade" id="modal-sm_{{ $cart->id }}">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <form action="{{ route('cart.update', $cart->id) }}" method="POST">
                                                            @csrf
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Ubah QTY Pemesanan</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">                                       
                                                                    <input type="number" min="1" value="{{ $cart->jumlah }}" placeholder="Jumlah pemesanan" name="jumlah">                                                                    
                                                                </div>
                                                                <div class="modal-footer justify-content-between">                                                                   
                                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <tr>
                                                <th>Total:</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td id="result">
                                                    @php
                                                        $totalWisata = 0;
                                                        $totalHotel = 0;
                                                        $totalKuliner = 0;
                                                        $sum = 0;

                                                        foreach($myCart as $cart){
                                                            if($cart->idKategoriData == 1){
                                                                $totalWisata = $cart->wisata()->first()->harga * $cart->jumlah;
                                                            }elseif($cart->idKategoriData == 2){
                                                                $totalHotel = $cart->hotel()->first()->harga * $cart->jumlah;
                                                            }else{
                                                                $totalKuliner = $cart->kuliner()->first()->harga * $cart->jumlah;
                                                            }
                                                            $sum += $totalAll = $totalWisata + $totalHotel + $totalKuliner;
                                                        }
                                                    @endphp
                                                    {{ $sum }}
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row no-print">
                            <div class="col-12">
                                <button type="button" data-toggle="modal" data-target="#modal-sm_{{ $cart->id }}" class="btn btn-success float-right"><i class="far fa-credit-card"></i>
                                    Bayar Sekarang
                                </button>
                            </div>
                        </div><br><br><br>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


