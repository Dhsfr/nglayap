@extends('admin.layouts.app')
@section('content')
@section('hotel', 'active')
@section('listHotel', 'active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Hotel</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('hotel.index') }}">Hotel</a></li>
                            <li class="breadcrumb-item active">Edit Hotel</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="img-fluid"
                                    src="@if($edit->foto!=null) {{asset('assets/img/hotel/'.$edit->foto)}} @else {{asset('assets/dist/img/user4-128x128.jpg')}} @endif">
                                </div>
                                <h3 class="profile-username text-center">{{ $edit->nama }} -> {{ $edit->bintangHotel }}</h3>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Harga</b> <a class="float-right">{{ $edit->harga }}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Rating</b> <a class="float-right">{{ $edit->rating }}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Alamat</b> <a class="float-right">{{ $edit->alamat }}</a>
                                    </li>
                                </ul>
                                <p class="text-muted">{{ $edit->deskripsi }}</p>
                            </div>
                        </div>
                    </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            Edit Wisata
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                                <div class="post" id="settings">
                                    <form role="form" action="{{ route('hotel.update', $edit->id) }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="exampleInputFile" class="col-sm-2 col-form-label">Foto</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="foto" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-2 col-form-label">Nama Hotel</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="name" placeholder="Masukkan Nama Hotel" name="nama" value="{{ $edit->nama }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" id="harga" placeholder="Masukkan Harga" name="harga" value="{{ $edit->harga }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Bintang Hotel</label>
                                            <div class="col-sm-10">
                                                <select class="custom-select" name="bintangHotel">
                                                    <option>Pilih Bintang</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Rating</label>
                                            <div class="col-sm-10">
                                                <select class="custom-select" name="rating">
                                                    <option>Pilih Rating</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label" for="alamat">Alamat</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="alamat" placeholder="Masukkan Alamat" name="alamat" value="{{ $edit->alamat }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                                            <div class="col-sm-10">
                                                <textarea id="deskripsi" name="deskripsi" placeholder="Masukkan Deskripsi" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $edit->deskripsi }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="offset-sm-2 col-sm-10">
                                                <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection