@extends('admin.layouts.app')

@section('extrahead')
    <link rel="stylesheet" href="{{ asset('assets/plugins/ekko-lightbox/ekko-lightbox.css') }}">
@endsection

@section('content')
@section('hotel', 'active')
@section('kamarHotel', 'active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Kamar Hotel</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active">Kamar Hotel</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="col-md-12">
                                <div class="card bg-gradient-success">
                                    <div class="card-header">
                                        <h3 class="card-title">Success</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        {{ session('status')}}
                                    </div>
                                </div>
                            </div>
                        @endif
                        @foreach($hotel as $hotels)
                            <div class="card card-primary">
                                <div class="card-header">
                                    <div class="card-title">{{ $hotels->nama }}</div>
                                    <a href="{{ route('hotel.createKamar', $hotels->id) }}" type="button" class="btn btn-info float-right"><i class="fas fa-plus"></i>Add Kamar Hotel</a>
                                </div>
                                <div class="card-body">
                                    <div class="filter-container p-0 row">
                                    @foreach($kamar as $kamars)
                                    @if($hotels->id == $kamars->idHotel)
                                        <div class="filtr-item col-sm-2">
                                            <p>{{ $kamars->nama }}</p>
                                            <a href="{{ route('hotel.editKamar', $kamars->id) }}">
                                                <img src="@if($kamars->foto!=null) {{asset('assets/img/kamarHotel/'.$kamars->foto)}} @endif" class="img-thumbnail" alt="white sample"/>
                                            </a>
                                            <a href="{{ route('hotel.editKamar', $kamars->id) }}"><i class="fas fa-edit"> Edit</i></a> ||
                                            <a href="" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin?', 'Konfirmasi Penghapusan Pengguna')){ $('form#hapus{{ $loop->iteration }}').submit(); }"><i class="fa fa-trash"> Hapus</i></a>
                                            <form id="hapus{{ $loop->iteration }}" action="{{ route('hotel.destroyKamar', $kamars->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            <br><br>
                                        </div>  
                                    @endif
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{ $hotel->links() }}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
<script src="{{ asset('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
<script src="{{ asset('assets/plugins/filterizr/jquery.filterizr.js') }}"></script>
<script>
    $(function () {
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });

            $('.filter-container').filterizr({gutterPixels: 3});
            $('.btn[data-filter]').on('click', function() {
            $('.btn[data-filter]').removeClass('active');
            $(this).addClass('active');
        });
    })
</script>
@endsection