@extends('admin.layouts.app')
@section('extrahead')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
@section('users','active')
@section('userAccount', 'active')
	<div class="content-wrapper">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1>User Account</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item active">User Account</li>
						</ol>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-12">
					@if (session('status'))
						<div class="col-md-12">
							<div class="card bg-gradient-success">
								<div class="card-header">
									<h3 class="card-title">Success</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									{{ session('status')}}
								</div>
							</div>
						</div>
					@endif
					<div class="card">
						<div class="card-header">
							<a href="{{ route('users.addAccount') }}" type="button" class="btn btn-info float-right"><i class="fas fa-plus"></i>Add Account</a>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Email</th>
										<th>Role</th>
										<th>Created_at</th>
										<th>Action</th>	
									</tr>
								</thead>
								<tbody>
									@foreach($userAccount as $account)
										<tr>
											<td>{{ $account->name }}</td>
											<td>{{ $account->email }}</td>
											<td>{{ $account->roles()->first()->name }}</td>
											<td>{{ $account->created_at }}</td>
											<td>
												<a href="{{ route('users.editAccount', $account->id) }}"><i class="fa fa-edit"></i></a>
												@if($account->roles()->first()->name != 'admin')
													||
													
													<a href="" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin?', 'Konfirmasi Penghapusan Pengguna')){ $('form#hapus{{ $loop->iteration }}').submit(); }"><i class="fa fa-trash"></i></a>
													<form id="hapus{{ $loop->iteration }}" action="{{ route('users.destroyAccount', ['userid' => $account->id]) }}" method="POST">
														@csrf
														@method('DELETE')
													</form>
												@endif
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('extrascript')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
		$(function () {
			$("#example1").DataTable();
			$('#example2').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			});
		});
    </script>
@endsection
