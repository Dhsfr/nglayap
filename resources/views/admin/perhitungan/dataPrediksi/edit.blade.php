@extends('admin.layouts.app')
@section('content')
@section('perhitungan','active')
@section('dataPrediksi', 'active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Data Prediksi</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('perhitungan.indexDataPrediksi') }}">Data Prediksi</a></li>
                            <li class="breadcrumb-item active">Edit Data Prediksi</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                            </div>
                            <form role="form" action="{{ route('perhitungan.updateDataPrediksi', $edit->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Lokasi Awal</label>
                                        <select class="custom-select" name="idLokasi">
                                            <option>Pilih Lokasi</option>
                                            @foreach($lokasi as $lokasi)
                                                <option {{$edit->idLokasi == $lokasi->id  ? 'selected' : ''}} value="{{ $lokasi->id }}">{{ $lokasi->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Pilih Kategori Data</label>
                                        <select class="custom-select" name="idKategoriData" id="idKategoriData">
                                            <option>-Select-</option>
                                            @foreach($kategoriData as $kategori)
                                                <option {{$edit->idKategoriData == $kategori->id  ? 'selected' : ''}} value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Pilih Nama Data</label>
                                        <select class="custom-select" name="idJenisData" id="idJenisData">
                                        @if($edit->kategoriData()->first()->nama == 'Wisata')
                                            <option value="{{ $edit->idJenisData }}">{{ $edit->wisata()->first()->nama }}</option>
                                        @elseif($edit->kategoriData()->first()->nama == 'Hotel')
                                            <option value="{{ $edit->idJenisData }}">{{ $edit->hotel()->first()->nama }}</option>
                                        @elseif($edit->kategoriData()->first()->nama == 'Kuliner')
                                            <option value="{{ $edit->idJenisData }}">{{ $edit->kuliner()->first()->nama }}</option>
                                        @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jarak">Jarak (Km)</label>
                                        <input type="number" class="form-control" id="jarak" placeholder="Masukkan Jarak" name="jarak" value="{{ $edit->jarak }}" required>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
    <script>
        $(function () {
            $('.textarea').summernote()
        })
    </script>
    <script>
        $("#idKategoriData").focusout(function(e){
        // alert($(this).val());
            var idKategoriData = $(this).val();
            $.ajax({
                type: "GET",
                url: "{{route('perhitungan.getDataPrediksi')}}",
                data: {'idKategoriData':idKategoriData},
                dataType: 'json',
                success : function(response) {
                    var len = response.length;
                    $("#idJenisData").empty();

                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['nama'];             
                        $("#idJenisData").append("<option value='"+id+"'>"+name+"</option>");
                    } 
                },
                error: function(response) {
                    alert(response.responseJSON.message);
                }
            });
        });
    </script>
@endsection

